(function (Drupal, drupalSettings) {
  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  Drupal.smartContent.plugin.Field['ipstack'] = function (condition) {
    let key = condition.field.pluginId.split(':')[1];
    if(!Drupal.smartContent.hasOwnProperty('ipstack')) {
      // todo: decide if this should update behind the scenes.
      if(!Drupal.smartContent.storage.isExpired('ipstack')) {
        let values = Drupal.smartContent.storage.getValue('ipstack');
        Drupal.smartContent.ipstack  = values;
      }
      else {
        Drupal.smartContent.ipstack = new Promise((resolve, reject) => {
          drupalSettings.ajaxTrustedUrl['/ajax/smart-content-ipstack/lookup'] = true;
          let ajaxObject = new Drupal.ajax({
            url: '/ajax/smart-content-ipstack/lookup',
            progress: false,
            dataType: 'json',
            success: function (response, status) {
              if(response.hasOwnProperty('ip')) {
                Drupal.smartContent.storage.setValue('ipstack', response);
              }
              resolve(response);
            }
          });
          ajaxObject.execute();
        });
      }

    }
    return Promise.resolve(Drupal.smartContent.ipstack).then( (value) => {
      // Get deep nested property based on key.
      let check = key.split('__').reduce((o, x) => o == undefined ? o : o[x]
        , value)
      return check;
    });
  }

}(Drupal, drupalSettings));

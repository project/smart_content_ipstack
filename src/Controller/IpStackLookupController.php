<?php

namespace Drupal\smart_content_ipstack\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class IpStackLookupController.
 */
class IpStackLookupController extends ControllerBase {

  use AjaxHelperTrait;

  /**
   * Guzzle client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Lookup.
   *
   * @return string
   *   Return Hello string.
   */
  public function lookup() {
    if($this->isAjax()) {
      $ipstack = \Drupal::service('ipstack');
      // We will manually tell it to override caching as we expect this can be
      // used on every visit by anonymous users.  JS will instead handle cache
      // and request on client side.
      $this->config('ipstack.settings')->setModuleOverride(['use_cache' => FALSE]);

      $is_localhost = TRUE;
      $ip = $is_localhost ? 'check' : \Drupal::request()->getClientIp();
      $ipstack->setIp($ip);

      $result = $ipstack->getData();
      $data = [];
      if (empty($result['error'])) {
        $result_data = json_decode($result['data'], true);
        if(!empty($result_data['type'])) {
          $data = $result_data;
        }
      }
      return new JsonResponse($data);
    }

    throw new AccessDeniedHttpException();
  }

}

<?php

namespace Drupal\smart_content_ipstack\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides condition plugin definitions for IpStack fields.
 *
 * @see Drupal\smart_content_ipstack\Plugin\smart_content\Condition\IpStackCondition
 */
class IpStackConditionDeriver extends DeriverBase implements
  ContainerDeriverInterface {

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;


  /**
   * IpStackConditionDeriver constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $ipstack_fields = $this->getStaticFields();
    foreach ($ipstack_fields as $key => $ipstack_field) {
      $this->derivatives[$key] = $ipstack_field + $base_plugin_definition;
    }
    return $this->derivatives;
  }


  /**
   * Function to return a static list of fields from ipstack.
   *
   * @return array
   *   An array of fields from IpStack.
   */
  protected function getStaticFields() {
    $config = $config = \Drupal::config('smart_content_ipstack.settings');
    $modules = $config->get('modules');

    // If not yet configured, add no conditions.
    if (empty($modules)) {
      return [];
    }

    $definitions = [
      "ip" => [
        'label' => 'IP',
        'type' => 'textfield',
      ],

      "type" => [
        'label' => 'IP Type',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getIpTypeOptions'],
      ],
      "continent_code" => [
        'label' => 'Continent Code',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getContinentCodeOptions'],
      ],
      "continent_name" => [
        'label' => 'Continent Name',
        'type' => 'textfield',
      ],
      "country_code" => [
        'label' => 'Country Code',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getCountryCodeOptions'],
      ],
      "country_name" => [
        'label' => 'Country Name',
        'type' => 'textfield',
      ],
      "region_code" => [
        'label' => 'Region Code',
        'type' => 'textfield',
      ],
      "region_name" => [
        'label' => 'Region Name',
        'type' => 'textfield',
      ],
      "city" => [
        'label' => 'City',
        'type' => 'textfield',
      ],
      "zip" => [
        'label' => 'Zip',
        'type' => 'textfield',
      ],
      // @todo: need to add a lat/lon type.
      "latitude" => [
        'label' => 'Latitude',
        'type' => 'number',
      ],
      "longitude" => [
        'label' => 'Longitude',
        'type' => 'number',
      ],
    ];

    if ($modules['location']) {
      $definitions += [
        "location__geoname_id" => [
          'label' => 'Location Geoname ID',
          'type' => 'number',
        ],
        "location__capital" => [
          'label' => 'Location Country Capital',
          'type' => 'textfield',
        ],
        "location__calling_code"  => [
          'label' => 'Location Calling Code',
          'type' => 'number',
        ],
        "location__is_eu"  => [
          'label' => 'Location is EU',
          'type' => 'boolean',
        ],
      ];
    }
    if ($modules['time_zone']) {
      $definitions += [
        "time_zone__id" => [
          'label' => 'Time Zone',
          'type' => 'select',
          'options_callback' => [get_class($this), 'getTimeZoneOptions'],
        ],
        // @todo: need to add a date/time type.
//        "time_zone__current_time" => "2018-03-29T07:35:08-07:00",
        "time_zone__gmt_offset" => [
          'label' => 'Time Zone GMT Offset(seconds)',
          'type' => 'number',
        ],
        "time_zone__code" => [
          'label' => 'Time Zone Code',
          'type' => 'select',
          'options_callback' => [get_class($this), 'getTimeZoneCodeOptions'],
        ],
        "time_zone__is_daylight_saving" => [
          'label' => 'Time Zone is Daylight Savings',
          'type' => 'boolean',
        ],
      ];
    }

    if ($modules['currency']) {
      $definitions += [
        "currency__code" => [
          'label' => 'Current Code',
          'type' => 'textfield',
        ],
        "currency__name" => [
          'label' => 'Current Name',
          'type' => 'textfield',
        ],
        // @todo: add support.
//        "currency.symbol" => "$",
//        "currency.symbol_native" => "$",
      ];
    }
    if ($modules['connection']) {
      $definitions += [
        "connection__asn" => [
          'label' => 'Connection ASN',
          'type' => 'number',
        ],
        "connection__isp" => [
          'label' => 'Connection ISP',
          'type' => 'textfield',
        ],
      ];
    }

    if ($modules['security']) {
      $definitions += [
        "security__is_proxy" => [
          'label' => 'Is Proxy',
          'type' => 'boolean',
        ],
        "security__proxy_type" => [
          'label' => 'Proxy Type',
          'type' => 'select',
          'options_callback' => [get_class($this), 'getProxyTypeOptions'],
        ],
        "security__is_crawler" => [
          'label' => 'Is Crawler',
          'type' => 'boolean',
        ],
        "security__crawler_name" => [
          'label' => 'Crawler Name',
          'type' => 'textfield',
        ],
        "security__crawler_type" => [
          'label' => 'Crawler Type',
          'type' => 'select',
          'options_callback' => [get_class($this), 'getCrawlerTypeOptions'],
        ],
        "security__is_tor" => [
          'label' => 'Is Tor',
          'type' => 'boolean',
        ],
        "security__threat_level" => [
          'label' => 'Thread Level',
          'type' => 'select',
          'options_callback' => [get_class($this), 'getThreatLevelOptions'],
        ],
        "security__threat_types" => [
          'label' => 'Threat Types',
          'type' => 'select',
          'options_callback' => [get_class($this), 'getThreatTypeOptions'],
        ],
      ];
    }
    return $definitions;
  }

  public static function getIpTypeOptions($plugin) {
    return [
      'ipv4' => 'IPv4',
      'ipv6' => 'IPv6',
    ];
  }

  public static function getContinentCodeOptions($plugin) {
    return [
      'AF' => 'Africa',
      'AS' => 'Asia',
      'EU' => 'Europe',
      'NA' => 'North America',
      'OC' => 'Oceania',
      'SA' => 'South America',
      'AN' => 'Antarctica',
    ];
  }

  public static function getCountryCodeOptions($plugin) {
    $country_list = \Drupal::service('country_manager')->getList();
    $options = [];
    foreach($country_list as $key => $name) {
      $options[$key] = $name . ' (' . $key . ')';
    }
    return $options;
  }

  public static function getTimeZoneOptions($plugin) {
    return system_time_zones(FALSE, TRUE);
  }

  public static function getTimeZoneCodeOptions($plugin) {
    $list = timezone_abbreviations_list();
    $codes = array_map('strtoupper', array_keys($list));
    return array_combine($codes, $codes);
  }

  public static function getProxyTypeOptions($plugin) {
    return [
      'cgi' => 'CGI',
      'web' => 'Web',
      'vpn' => 'VPN',
    ];
  }
  
  public static function getCrawlerTypeOptions($plugin) {
    return [
      'unrecognized' => 'Unrecognized',
      'search_engine_bot' => 'Search engine bot',
      'site_monitor' => 'Site monitor',
      'screenshot_creator' => 'Screenshot creator',
      'link_checker' => 'Link checker',
      'wearable_computer' => 'Wearable computer',
      'web_scraper' => 'Web scraper',
      'vulnerability_scanner' => 'Vulnerability scanner',
      'virus_scanner' => 'Virus scanner',
      'speed_tester' => 'Speed tester',
      'feed_fetcher' => 'Feed Fetcher',
      'tool' => 'Tool',
      'marketeing' => 'Marketing',
    ];
  }
  public static function getThreatLevelOptions($plugin) {
    return [
      'low' => 'Low',
      'medium' => 'Medium',
      'high' => 'High',
    ];
  } 
  public static function getThreatTypeOptions($plugin) {
    return [
      'tor' => 'Tor System',
      'fake_crawler' => 'Fake Crawler',
      'web_scraper' => 'Web Scraper',
      'attack_source' => 'Attack Source identified: HTTP',
      'attack_source_http' => 'Attack Source identified: HTTP',
      'attack_source_mail' => 'Attack Source identified: Mail',
      'attack_source_ssh' => 'Attack Source identified: SSH',
    ];
  }
}

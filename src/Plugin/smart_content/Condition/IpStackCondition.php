<?php

namespace Drupal\smart_content_ipstack\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a IpStack condition plugin.
 *
 * @SmartCondition(
 *   id = "ipstack",
 *   label = @Translation("ipstack"),
 *   group = "ipstack",
 *   deriver = "Drupal\smart_content_ipstack\Plugin\Derivative\IpStackConditionDeriver"
 * )
 */
class IpStackCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['smart_content_ipstack/condition.ipstack']));
    return $libraries;
  }

}

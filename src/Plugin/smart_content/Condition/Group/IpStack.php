<?php


namespace Drupal\smart_content_ipstack\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a condition group for IpStack conditions.
 *
 * @SmartConditionGroup(
 *   id = "ipstack",
 *   label = @Translation("ipstack")
 * )
 */
class IpStack extends ConditionGroupBase {

}

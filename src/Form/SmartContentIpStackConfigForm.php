<?php

namespace Drupal\smart_content_ipstack\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SmartContentIpStackConfigForm.
 */
class SmartContentIpStackConfigForm extends ConfigFormBase {

  /**
   * The ipstack service.
   *
   * @var \Drupal\ipstack\Ipstack
   */
  protected $ipStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->ipStack = $container->get('ipstack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'smart_content_ipstack.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smart_content_ip_stack_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('smart_content_ipstack.settings');

    $form['modules'] = [
      '#type' => 'fieldset',
      '#title' => 'IP Stack Modules',
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'modules-wrapper'
      ]
    ];

    // TODO: add automatic module detection.
    $module_config = $config->get('modules');
    $form['modules']['description'] = [
      '#type' => '#markup',
      '#markup' => 'Select which modules your subscription includes, and should be made available to Smart Content.  It is important these are configured correctly as this affects the client side caching strategy of Smart Content.'
    ];


    $form['modules']['description'] = [
      '#type' => 'submit',
      '#value' => t('Detect'),
      '#name' => 'detect_modules',
      '#submit' => [[$this, 'detectModules']],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'detectModulesAjax'],
        'wrapper' => 'modules-wrapper',
      ],
    ];
    $form['modules']['location'] = [
      '#title' => 'Location',
      '#type' => 'checkbox',
      '#description' => 'Use ipstack\'s extensive set of localization data to implement geographic restrictions on your site, optimize ad targeting or deliver user experiences customized based on the location of your website visitors.',
      '#default_value' => isset($module_config['location']) ? $module_config['location'] : TRUE,
    ];
    $form['modules']['currency'] = [
      '#title' => 'Currency',
      '#type' => 'checkbox',
      '#description' => 'Get instant and accurate information about the primary currency used in the location returned for the processed IP address and deliver a tailored shopping experience to your customers.',
      '#default_value' => isset($module_config['currency']) ? $module_config['currency'] : FALSE,
    ];
    $form['modules']['time_zone'] = [
      '#title' => 'Time Zone',
      '#type' => 'checkbox',
      '#description' => 'Find out about the time zone your users are located in without the need for them to fill out any forms, and act accordingly based on the time-related metadata returned by the ipstack API.',
      '#default_value' => isset($module_config['time_zone']) ? $module_config['time_zone'] : FALSE,
    ];
    $form['modules']['connection'] = [
      '#title' => 'Connection',
      '#type' => 'checkbox',
      '#description' => 'Make use of valuable information about the ASN and the hostname of the ISP your website visitors are using.',
      '#default_value' => isset($module_config['connection']) ? $module_config['connection'] : FALSE,
    ];
    $form['modules']['security'] = [
      '#title' => 'Security',
      '#type' => 'checkbox',
      '#description' => 'Protect your site and web application and always be a step ahead of potential threats to your business by detecting proxies, crawlers or tor users at first glance.',
      '#default_value' => isset($module_config['security']) ? $module_config['security'] : FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Provides a '#submit' callback for detecting settings..
   */
  public function detectModules(array &$form, FormStateInterface $form_state) {
    $inputs = $form_state->getUserInput();
    if ($data = $this->getIpStackData()) {
        foreach($inputs['modules'] as $key => $value) {
          $inputs['modules'][$key] = !empty($data[$key]) ? '1' : null;
        }
        $form_state->setUserInput($inputs);
    }
    $form_state->setRebuild();
  }

  protected function getIpStackData() {
    // We will manually tell it to override caching as we expect this can be
    // used on every visit by anonymous users.  JS will instead handle cache
    // and request on client side.
    $this->config('ipstack.settings')->setModuleOverride(['use_cache' => FALSE]);

    $is_localhost = TRUE;
    $ip = $is_localhost ? 'check' : \Drupal::request()->getClientIp();
    $this->ipStack->setIp($ip);
    $result = $this->ipStack->getData();
    $data = [];
    if (empty($result['error'])) {
      $result_data = json_decode($result['data'], TRUE);
      if (!empty($result_data['type'])) {
        return $result_data;
      }
    }
    return [];
  }
  /**
   * Provides an '#ajax' callback for detecting settings..
   */
  public function detectModulesAjax(array &$form, FormStateInterface $form_state) {
    return $form['modules'];
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // TODO: Should we validate against the API before save?


    // TODO: Determine if we should hash based on settings so we can safely
    //   rebuild cached data if user upgrades subscription.

    $this->config('smart_content_ipstack.settings')
      ->set('modules', $form_state->getValue('modules'))
      ->save();
  }

}

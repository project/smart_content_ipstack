<?php

namespace Drupal\smart_content_ipstack\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the smart_content_ipstack module.
 */
class IpStackLookupControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "smart_content_ipstack IpStackLookupController's controller functionality",
      'description' => 'Test Unit for module smart_content_ipstack and controller IpStackLookupController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests smart_content_ipstack functionality.
   */
  public function testIpStackLookupController() {
    // Check that the basic functions of module smart_content_ipstack.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
